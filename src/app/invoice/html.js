require("dotenv").config();
const moment = require("moment");

const formatDate = (invoiceDate) => {
  const dayNames = {
    Sunday: "Minggu",
    Monday: "Senin",
    Tuesday: "Selasa",
    Wednesday: "Rabu",
    Thursday: "Kamis",
    Friday: "Jumat",
    Saturday: "Sabtu",
  };

  const day = dayNames[moment(invoiceDate).format("dddd")];

  return `${day}, ${moment(invoiceDate).format("DD-MM-YYYY")}`;
};

const turnIntoCurrency = (val) => {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
  }).format(val);
};

const HTML_STRING = (invoice, company) => {
  let shouldShowStamp = false;
  let stampImage = "";
  if ([2, 3].includes(invoice.invoice_status_id)) {
    shouldShowStamp = true;
    const images = {
      2: "selesai.png",
      3: "lunas.png",
    };

    stampImage = images[invoice.invoice_status_id];
  }

  return `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body style="font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif; margin: 0;">
    <header style="width:100%; overflow-x: hidden; position: relative; z-index: 0;">
        <img src="https://oyhmqxylcaqizqcroxkt.supabase.co/storage/v1/object/public/assets/wave-vector.png" alt="header vector" style="height: 100px;width:100%" />
    </header>

    <div id="invoice-pdf" style="padding: 0px 10px 25px; margin: -30px 15px 0px; z-index: 99;">
        <div id="invoice-header" style="display: -webkit-flex; -webkit-flex-direction: row; -webkit-flex: 1; gap: 4; justify-content: space-between; align-items: start; width: 100%; margin-bottom: 35px; -webkit-justify-content: space-between">
            <div style="width:30%">
              <img src="https://oyhmqxylcaqizqcroxkt.supabase.co/storage/v1/object/public/logo/ai-fashion.png?t=20 Pcs24-04-14T09%3A53%3A16.196Z" style="width: 100%;" />
            </div>

            <div style="margin-left: 20 Pcspx; font-size: 2vw; width: 60%;">
                <table style="text-align: left; width: 60%;">
                    ${company?.stores.map(
                      (store) => `
                    <tr class="padding-top: 40px;">
                        <td style="font-weight: bolder; text-align: left;">${store?.name}</td>
                        <td>: ${store?.address}</td>
                    </tr>`
                    )}
                    <tr class="padding-top: 40px;">
                        <td style="font-weight: bolder; text-align: left;">Office</td>
                        <td>: ${company?.address}</td>
                    </tr>
                    <tr class="padding-top: 40px;">
                        <td style="font-weight: bolder; text-align: left;">Telp/WA</td>
                        <td>: ${company?.phone}</td>
                    </tr>
                    <tr class="padding-top: 40px;">
                        <td style="font-weight: bolder; text-align: left;">Email</td>
                        <td>: ${company?.email}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="display: -webkit-flex; -webkit-flex-direction: row; -webkit-flex: 1; gap: 4; justify-content: space-between; align-items: start; font-size: 2vw; margin-top: 5em; width:100%;-webkit-justify-content: space-between" id="invoice-receiver">
            <table style="width:40%;border-left: 3px solid #915FDD; padding-left: 25px;">
                <tr>
                    <td style="font-weight: bolder; text-align: left;">Kepada Yth.</td>
                    <td>: ${invoice?.profiles?.fullname}</td>
                </tr>

                <tr>
                    <td style="font-weight: bolder; text-align: left;">Telp/WA</td>
                    <td>: ${invoice?.profiles?.phone ?? "-"}</td>
                </tr>

                <tr>
                    <td style="font-weight: bolder; text-align: left;">Alamat</td>
                    <td>: ${invoice?.profiles?.address ?? "-"}</td>
                </tr>
            </table>

            <table>
                <tr>
                    <td style="font-weight: bolder; text-align: left;">No Nota :</td>
                    <td style="text-align: right;">#${
                      invoice?.invoice_number
                    }</td>
                </tr>

                <tr>
                    <td style="font-weight: bolder; text-align: left;">Tanggal :</td>
                    <td style="text-align: right;">${formatDate(
                      invoice?.invoice_date
                    )}</td>
                </tr>
            </table>
        </div>

        ${
          shouldShowStamp
            ? `
            <div style="position: relative; width: 100%; height: auto;">
              <img
                src="${process.env.APP_URL}/image/${stampImage}"
                alt="Paid Watermark"
                style="
                position: absolute;
                rotate: 30deg;
                z-index: 100;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                opacity: 0.5;
              "
              />
            </div>
          `
            : ""
        }

        <div id="invoice-items" style="margin-top: 5em; width: 100%; font-size: 2vw;">
            <table style="width: 100%; border-collapse: collapse;">
                    <tr style="text-align: left;">
                        <th style="padding: 1em 2em; background-color: #313131; color: #fff;">Deskripsi</th>
                        <th style="padding: 1em 2em; width: 0.3rem;background-color: #313131; color: #fff;">Kuantitas</th>
                        <th style="padding: 1em 2em; width: 0.3rem;background-color: #313131; color: #fff;">Harga</th>
                        <th style="padding: 1em 2em; text-align: center;background-color: #313131; color: #fff;">Jumlah</th>
                    </tr>

                <tbody>
                    <tr>
                        <td style="font-weight: bold; padding: 1em 1em;">Pengambilan Barang</td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                    </tr>

                    ${invoice?.invoice_details.map(
                      (invoiceDetail) => `

                    <tr>
                        <td style="padding: 1em 1em;">${
                          invoiceDetail?.products?.name
                        }</td>
                        <td style=" padding: 1em 1em; text-align: center;">${
                          invoiceDetail?.qty
                        } Pcs</td>
                        <td style=" padding: 1em 1em; text-align: center;">
                        ${turnIntoCurrency(
                          invoiceDetail.total_price / invoiceDetail.qty
                        )}
                        </td>
                        <td style=" padding: 1em 1em; text-align: center;">
                        ${turnIntoCurrency(invoiceDetail?.total_price ?? 0)}
                        </td>
                    </tr>
                                                   `
                    )}

                    <tr>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style="font-weight:bold; padding: 1em 1em;">Subtotal</td>
                        <td style=" padding: 1em 1em; text-align: center;">${turnIntoCurrency(
                          invoice?.subtotal_product
                        )}</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold; padding: 1em 1em;">Penggabungan Nota</td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                    </tr>

                    ${invoice?.invoice_merges.map(
                      (invoiceMerge) => `
                    <tr>
                        <td style="padding: 1em 1em;">
                        Nota ${invoiceMerge?.invoices.invoice_date} - #${
                        invoiceMerge?.invoices.invoice_number
                      }
                        </td>
                        <td style=" padding: 1em 1em; text-align: center;"></td>
                        <td style=" padding: 1em 1em; text-align: center;"></td>
                        <td style=" padding: 1em 1em; text-align: center;">${turnIntoCurrency(
                          invoiceMerge?.remaining_amount
                        )}</td>
                    </tr>
                                                                      `
                    )}

                    <tr>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style="font-weight:bold; padding: 1em 1em;">Subtotal</td>
                        <td style=" padding: 1em 1em; text-align: center;">${turnIntoCurrency(
                          invoice?.subtotal_merge
                        )}</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold; padding: 1em 1em;">Retur Barang</td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                    </tr>

                    ${invoice?.invoice_returns.map(
                      (invoiceReturn) => `
                    <tr>
                        <td style="padding: 1em 1em;">${
                          invoiceReturn?.products?.name
                        }</td>
                        <td style=" padding: 1em 1em; text-align: center;">${
                          invoiceReturn?.qty
                        } Pcs</td>
                        <td style=" padding: 1em 1em; text-align: center;">
                        ${turnIntoCurrency(
                          invoiceReturn?.total_price ??
                            0 / invoiceReturn?.qty ??
                            0
                        )}
                        </td>
                        <td style=" padding: 1em 1em; text-align: center;">${turnIntoCurrency(
                          invoiceReturn?.total_price
                        )}</td>
                    </tr>
                                                                            `
                    )}


                    <tr>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style="font-weight:bold; padding: 1em 1em;">Subtotal</td>
                        <td style=" padding: 1em 1em; text-align: center;">${turnIntoCurrency(
                          invoice?.subtotal_return
                        )}</td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold; padding: 1em 1em;">Pembayaran</td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                    </tr>

                    ${invoice?.invoice_payments.map(
                      (invoicePayment) => `
                    <tr>
                        <td style="padding: 1em 1em;">

                        ${formatDate(invoicePayment?.paid_at)} - ${
                        invoicePayment?.payment_method
                      } ${invoicePayment?.bank_account_name ?? ""}
                        </td>
                        <td style=" padding: 1em 1em; text-align: center;"></td>
                        <td style=" padding: 1em 1em; text-align: center;"></td>
                        <td style=" padding: 1em 1em; text-align: center;">${turnIntoCurrency(
                          invoicePayment?.amount
                        )}</td>
                    </tr>
                                                                              `
                    )}

                    <tr>
                        <td style=" padding: 1em 1em;"></td>
                        <td style=" padding: 1em 1em;"></td>
                        <td style="font-weight:bold; padding: 1em 1em;">Subtotal</td>
                        <td style=" padding: 1em 1em; text-align: center;">${turnIntoCurrency(
                          invoice?.paid_amount
                        )}</td>
                    </tr>

                    <tr>
                        <td style=" padding: 1em 1em;background-color: #ebe12a;"></td>
                        <td style=" padding: 1em 1em;background-color: #ebe12a;"></td>
                        <td style="font-weight:bold; padding: 1em 1em;background-color: #ebe12a;">TOTAL</td>
                        <td style=" padding: 1em 1em; text-align: center;background-color: #ebe12a;">${turnIntoCurrency(
                          invoice?.remaining_amount
                        )}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div id="invoice-warning-text" style="margin-top: 100px">
            <p style="font-weight: bolder; font-size: 2vw; line-height: 1px;">PERHATIAN !</p>
            <p style="font-weight: lighter; font-size: 2vw;">
                Barang yang sudah dibeli tidak dapat ditukar/dikembalikan.
            </p>
        </div>

        <div id="invoice-payment-method" style="margin: 30px 0px; font-size: 2vw;">
            <table>
              ${company?.bank_accounts.map(
                (bankAccount) => `
                <tr>
                    <td style="font-weight: bolder;">
                        ${bankAccount?.bank}
                    </td>
                    <td>
                        : ${bankAccount?.accountNo} - A/N ${bankAccount?.pic}
                    </td>
                </tr>
              `
              )}

            </table>
        </div>
    </div>
</body>
</html>`;
};

module.exports = {
  HTML_STRING,
};
