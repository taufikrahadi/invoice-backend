const { supabaseClient } = require("../../config/supabase");
const { getUrlParam } = require("../../utils/http/get-url-param");
const { HTML_STRING } = require("./html");
const { generateInvoicePdf, getInvoiceByID } = require("./service");

const generatePdfInvoice = async (req, res, next) => {
  const id = getUrlParam(req, "id", null);
  const supabase = supabaseClient();
  try {
    const { filename, file } = await generateInvoicePdf(id, supabase);
    // Set headers to indicate a PDF file
    res.setHeader("Content-Type", "application/pdf");
    res.setHeader("Content-Disposition", `attachment; filename="${filename}"`); // Change "inline" to "attachment" for download
    // Send the PDF file buffer as the response
    res.send(file);
    return;
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const testPdfInvoiceHtml = async (req, res, net) => {
  const id = getUrlParam(req, "id", null);
  const supabase = supabaseClient();
  const { invoice, company, invoiceMerges } = await getInvoiceByID(
    id,
    supabase
  );
  res.json({ invoice: { ...invoice, invoice_merges: invoiceMerges }, company });
};

module.exports = {
  generatePdfInvoice,
  testPdfInvoiceHtml,
};
