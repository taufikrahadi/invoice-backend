require("dotenv").config();
const pdf = require("html-pdf");
const { HTML_STRING } = require("./html");
const fs = require("fs/promises");
const moment = require("moment");

const INVOICE_PATH = process.env.INVOICE_PATH;

const getInvoiceByID = async (id, supabase) => {
  try {
    const fetchInvoice = async () => {
      const { data } = await supabase
        .from("invoices")
        .select(
          "*, profiles(id, fullname, code, phone, address), invoice_statuses(id,name)"
        )
        .eq("id", id)
        .single();

      return data;
    };

    const fetchInvoiceDetails = async () => {
      const { data } = await supabase
        .from("invoice_details")
        .select("*, products(id,name,sku)")
        .eq("invoice_id", id)
        .order("id", { ascending: true });

      return data;
    };

    const fetchInvoiceReturns = async () => {
      const { data } = await supabase
        .from("invoice_returns")
        .select("*, products(id,name,sku)")
        .eq("invoice_id", id)
        .order("id", { ascending: true });

      return data;
    };

    const fetchInvoicePayments = async () => {
      const { data } = await supabase
        .from("invoice_payments")
        .select("*")
        .eq("invoice_id", id)
        .order("id", { ascending: true });

      return data;
    };

    const fetchInvoiceMerges = async () => {
      const { data } = await supabase
        .from("invoice_merges")
        .select(
          "*, invoices:from_invoice_id(id, code, invoice_number, invoice_date)"
        )
        .eq("to_invoice_id", id)
        .order("id", { ascending: true });

      return data;
    };

    const fetchCompany = async () => {
      const { data } = await supabase
        .from("companies")
        .select("*")
        .eq("id", 1)
        .single();

      return data;
    };

    const [
      invoice,
      invoiceMerges,
      company,
      invoiceDetails,
      invoicePayments,
      invoiceReturns,
    ] = await Promise.all([
      fetchInvoice(),
      fetchInvoiceMerges(),
      fetchCompany(),
      fetchInvoiceDetails(),
      fetchInvoicePayments(),
      fetchInvoiceReturns(),
    ]);

    return {
      invoice: {
        ...invoice,
        invoice_details: invoiceDetails,
        invoice_payments: invoicePayments,
        invoice_returns: invoiceReturns,
      },
      invoiceMerges,
      company,
    };
  } catch (error) {
    throw error;
  }
};

const generateInvoicePdf = async (invoiceId, supabase) => {
  try {
    const { invoice, company, invoiceMerges } = await getInvoiceByID(
      invoiceId,
      supabase
    );

    if (!invoice) throw new Error("Invoice not found");

    if (process.env.SAVE_INVOICE) {
      const fileExists = await localHasInvoiceFile(
        `${INVOICE_PATH}${invoice.code}.pdf`
      );

      if (fileExists) {
        return {
          file: await fs.readFile(`${INVOICE_PATH}${invoice.code}.pdf`),
          filename: `${invoice.code}.pdf`,
        };
      }
    }

    const html = HTML_STRING(
      { ...invoice, invoice_merges: invoiceMerges },
      company
    );

    const pdfBuffer = await new Promise((resolve, reject) => {
      pdf
        .create(html, {
          orientation: "portrait",
          format: "",
          header: {
            width: "10in",
            height: "0",
          },
        })
        .toBuffer((err, pdfBuffer) => {
          if (err) {
            reject(err);
          }

          resolve(pdfBuffer);
        });
    });

    const filename = generateFilename(
      invoice.profiles.code,
      invoice.invoice_number,
      invoice.invoice_date
    );
    // const filepath = `${INVOICE_PATH}${filename}`;

    return { file: pdfBuffer, filename };
  } catch (error) {
    throw error;
  }
};

const generateFilename = (profileCode, invoiceNo, invoiceDate) => {
  const formattedDate = moment(invoiceDate).format("DDMMYYYY");

  return `INV${profileCode}-${invoiceNo}-${formattedDate}.pdf`;
};

/**
 *
 * @param {string} filepath
 * @returns Boolean
 */
const localHasInvoiceFile = async (filepath) => {
  try {
    await fs.access(filepath);
    return true;
  } catch (error) {
    return false;
  }
};

/**
 *
 * @param {string} filepath
 *
 * return buffer from file
 */
const getLocalInvoiceFile = async (filepath) => {
  try {
    const buffer = await fs.readFile(filepath);

    return buffer;
  } catch (error) {
    throw new Error(`failed getting file: ${error.message}`);
  }
};

/**
 *
 * @param {string} filepath
 * @param {Buffer} input
 */
const saveFileToLocal = async (filepath, input) => {
  try {
    await fs.writeFile(filepath, input);

    return;
  } catch (error) {
    throw new Error("failed saving file: " + error.message);
  }
};

module.exports = {
  generateInvoicePdf,
  getInvoiceByID,
};
