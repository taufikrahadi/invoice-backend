const router = require("express").Router();
const { generatePdfInvoice, testPdfInvoiceHtml } = require("./controller");

router.get("/:id/pdf", generatePdfInvoice);
router.get("/:id/view", testPdfInvoiceHtml);

module.exports = router;
