 const getUrlParam = (req, name, defaultValue) => {
  const value = req.params[name]

  return value ?? defaultValue
}

module.exports = {
  getUrlParam
}
