const http = require('http')
const API = require ('./config/api')

API.set("port", 3030)

const server = http.createServer(API)
server.listen(3030, () => {
  console.log("Running on port 3030")
})

