const express = require("express");
const cors = require("cors");
const invoiceRouter = require("../app/invoice/router");
const morgan = require("morgan");
const { join } = require("path");
const { cwd } = require("process");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  express.static(join(cwd(), "asset"), {
    maxAge: 60000,
  })
);
app.use(
  cors({
    origin: ["*"],
  })
);
app.use(morgan("combined"));

app.use("/api/v1/invoice", invoiceRouter);
app.use("*", (req, res, next) => {
  res.status(404).json({
    status: 404,
    message: "Resource not found!",
  });
});

module.exports = app;
