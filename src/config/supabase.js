require('dotenv').config()
const {createClient} = require('@supabase/supabase-js')

 const supabaseClient = () => {
  const client = createClient(process.env.SUPABASE_URL, process.env.SUPABASE_KEY)

  return client
}

module.exports = {
  supabaseClient
}
